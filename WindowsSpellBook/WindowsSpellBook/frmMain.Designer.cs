﻿namespace WindowsSpellBook {
    partial class frmMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblSpells = new System.Windows.Forms.Label();
            this.lstSpells = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lblSpells
            // 
            this.lblSpells.AutoSize = true;
            this.lblSpells.Location = new System.Drawing.Point(12, 42);
            this.lblSpells.Name = "lblSpells";
            this.lblSpells.Size = new System.Drawing.Size(35, 13);
            this.lblSpells.TabIndex = 0;
            this.lblSpells.Text = "Spells";
            // 
            // lstSpells
            // 
            this.lstSpells.FormattingEnabled = true;
            this.lstSpells.Location = new System.Drawing.Point(15, 58);
            this.lstSpells.Name = "lstSpells";
            this.lstSpells.Size = new System.Drawing.Size(146, 407);
            this.lstSpells.TabIndex = 1;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 596);
            this.Controls.Add(this.lstSpells);
            this.Controls.Add(this.lblSpells);
            this.Name = "frmMain";
            this.Text = "Spell Book";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblSpells;
        private System.Windows.Forms.ListBox lstSpells;
    }
}

