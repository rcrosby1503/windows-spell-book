﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Data.SqlClient;

namespace WindowsSpellBook {
    public partial class frmMain : Form {
        string connectionString;
        SqlConnection connection;

        public frmMain() {
            InitializeComponent();

            connectionString = ConfigurationManager.ConnectionStrings["WindowsSpellBook.Properties.Settings.SpellBookConnectionString"].ConnectionString;
        }

        private void Form1_Load(object sender, EventArgs e) {
            PopulateSpells();
        }

        private void PopulateSpells() {
            using (connection = new SqlConnection(connectionString))
            using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT * FROM Test4 ORDER BY Name ASC", connection)) {
                DataTable spellTable = new DataTable();
                adapter.Fill(spellTable);

                lstSpells.DisplayMember = "Name";
                lstSpells.ValueMember = "Id";
                lstSpells.DataSource = spellTable;
            }
        }
    }
}
